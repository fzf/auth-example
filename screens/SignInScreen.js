import React, { Component } from "react";
import { Alert, Button, StyleSheet, Text, View, TouchableOpacity, TextInput } from "react-native";
import Env from '../constants/Environment'

class SignInScreen extends React.Component {
  static navigationOptions = {
    title: 'Sign in',
  };

  constructor() {
    super();

    this.state = { phone: '2023805554' };
  }

  requestCode = async () => {
    try {
      let response = await fetch(
        Env.authUrl + '/users/send_code',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(
            {
              phone: this.state.phone,
              authorization: {
                client_id: '52ded0bb7dde06f9ac7c30a58fd340b50fe67bf70d2e02f57733bed0d68814ef',
                redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
                response_type: 'code'
              }
            }
          ),
        }
      );
      let responseJson = await response.json();
      this.props.navigation.push(
        'ConfirmationScreen',
        {
          phone: this.state.phone,
          code: responseJson.code
        }
      )
      return responseJson;
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <View>
        <Text>
          Enter phone number
        </Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />

        <Button
          title="Send Code"
          onPress={this.requestCode}
        />
      </View>
    );
  }
}

export default SignInScreen
