import React, { Component } from "react";
import { Alert, StyleSheet, Text, View, TouchableOpacity, Button, TextInput, AsyncStorage } from "react-native";
import Env from '../constants/Environment'

class ConfirmationScreen extends React.Component {
  static navigationOptions = {
    title: 'Please enter confirmation code',
  };
  constructor(props) {
    super(props);
    const { navigation } = this.props;
    const phone = navigation.getParam('phone', null);
    const code = navigation.getParam('code', null);

    this.state = { code: code, phone: phone };
  }

  render() {
    return (
      <View>
        <Text>
          Text message sent to {this.state.phone}.
        </Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(code) => this.setState({code})}
          value={this.state.code}
          />
        <Button title="Sign in!" onPress={this._signInAsync} />
      </View>
    );
  }

  _signInAsync = async () => {
    try {
      let codeResponse = await fetch(
        Env.authUrl + '/users/sign_in',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(
            {
              user: {
                phone: this.state.phone,
                password: this.state.code
              },
              client_id: '52ded0bb7dde06f9ac7c30a58fd340b50fe67bf70d2e02f57733bed0d68814ef',
              redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
              response_type: 'code'
            }
          ),
        }
      );
      let codeResponseJson = await codeResponse.json();
      let tokenResponse = await fetch(
        Env.authUrl + '/oauth/token',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(
            {
              grant_type: 'authorization_code',
              code: codeResponseJson.redirect_uri.code,
              client_id: '52ded0bb7dde06f9ac7c30a58fd340b50fe67bf70d2e02f57733bed0d68814ef',
              redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
              client_secret: '145f3dbae14dddc0b9a333001de444b6a4302057710a27ed238ee89f23011377'
            }
          )
        }
      );
      let tokenResponseJson = await tokenResponse.json();
      await AsyncStorage.setItem('accessToken', tokenResponseJson.access_token);
      this.props.navigation.navigate('Main');
    } catch (error) {
      console.error(error);
    }
  }
}

export default ConfirmationScreen
