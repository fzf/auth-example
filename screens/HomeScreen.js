import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  AsyncStorage,
  Button,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { MonoText } from '../components/StyledText';
import Env from '../constants/Environment'
import { ApolloProvider, graphql } from 'react-apollo';
import gql from 'graphql-tag';

const dogQuery = gql`
  query {
    users {
      phone
    }
  }
`;

const UserComponent = graphql(dogQuery)(props => {
  const { error, users } = props.data;
  if (error) {
    return <Text>{error.message}</Text>;
  }
  if (users) {
    return <Text>{users[0].phone}</Text>;
  }

  return <Text>Loading...</Text>;
});

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Welcome to the app!',
  };

  render() {
    return (
      <View>
        <Button title="Sign Out" onPress={this._signOutAsync} />
        <UserComponent/>
      </View>
    );
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };
}

export default HomeScreen
