import Constants from 'expo-constants'

const ENV = {
  dev: {
    authUrl: 'http://lvh.me:3000'
  },
  staging: {
    apiUrl: 'https://staging.orchard.ai/api'
  },
  prod: {
    apiUrl: 'https://orchard.ai/api'
  }
}

function getEnvVars(env = '') {
  if (env === null || env === undefined || env === '') return ENV.dev
  if (env.indexOf('dev') !== -1) return ENV.dev
  if (env.indexOf('staging') !== -1) return ENV.staging
  if (env.indexOf('prod') !== -1) return ENV.prod
}


export default getEnvVars(Constants.manifest.releaseChannel)
